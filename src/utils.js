import {ROWS, COLUMNS, GUN_POSITIONS} from "./config";

export const initialSeed = ({rows = ROWS, columns = COLUMNS, withGun = true} = {}) => {
    const cells = [];
    for (let i = 0; i < rows; i++) {
        cells[i] = [];
        for (let j = 0; j < columns; j++) {
            cells[i][j] = (withGun ? (i > 30 || j > 60) : true) && Math.random() > 0.6;
        }
    }
    if (withGun) {
        GUN_POSITIONS.forEach(cell => cells[cell[0]][cell[1]] = true);
    }
    return cells;
};

export const nextGeneration = cells => cells.map((row, i) => row.map((cell, j) => {
    const upperRow = cells[i-1]
        ? [cells[i-1][j-1], cells[i-1][j], cells[i-1][j+1]]
        : [];

    const sameRow = [cells[i][j-1], cells[i][j+1]];

    const bottomRow = cells[i+1]
        ? [cells[i+1][j-1], cells[i+1][j], cells[i+1][j+1]]
        : [];

    const aliveNeighbors = [...upperRow, ...sameRow, ...bottomRow].filter(x => x).length;
        
    return aliveNeighbors === 3 || (cell && aliveNeighbors === 2);
}));
