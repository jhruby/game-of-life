import {nextGeneration} from "./utils";

it("generates next iterations", () => {
    const gen0 = [
        [false, false, false],
        [true, true, true],
        [true, false, true],
    ];
    const gen1 = [
        [false, true, false],
        [true, false, true],
        [true, false, true],
    ];
    const gen2 = [
        [false, true, false],
        [true, false, true],
        [false, false, false],
    ];
    const gen3 = [
        [false, true, false],
        [false, true, false],
        [false, false, false],
    ];
    const gen4 = [
        [false, false, false],
        [false, false, false],
        [false, false, false],
    ];

    expect(nextGeneration(gen0)).toEqual(gen1);
    expect(nextGeneration(gen1)).toEqual(gen2);
    expect(nextGeneration(gen2)).toEqual(gen3);
    expect(nextGeneration(gen3)).toEqual(gen4);

    expect(
        nextGeneration(nextGeneration(nextGeneration(nextGeneration(gen0))))
    ).toEqual(gen4);
});
