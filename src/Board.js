import React, {Component} from "react";
import throttle from "raf-throttle";
import isEqual from "lodash.isequal";
import "./Board.css";
import {initialSeed, nextGeneration} from "./utils";

const getInitialState = () => {
  const cells = initialSeed();
  const nextCells = nextGeneration(cells);
  return { cells, nextCells };
}

class Board extends Component {  
    state = getInitialState();
    
    iterate = () => {
        const next = nextGeneration(this.state.nextCells);
        
        if(isEqual(next, this.state.cells)){
          if(!window.confirm('Game over! Click OK to restart')){
            cancelAnimationFrame(this.timer);
            return;
          }
          this.setState(getInitialState());
        } else {
          this.setState(state => ({
              cells: state.nextCells,
              nextCells: next,
          }));
        }
        this.timer = requestAnimationFrame(this.throttledIterate);
    }
    throttledIterate = throttle(this.iterate);
    
    renderRow = (cell, j) => (
        <td key={`column-${j}`} className={`cell ${cell ? "cell--alive" : ""}`}></td>
    );
    
    renderCells = (row, i) => (
        <tr key={`row-${i}`}>{row.map(this.renderRow)}</tr>
    );
    
    componentDidMount() {
        this.timer = requestAnimationFrame(this.throttledIterate);
    }
    
    componentWillUnmount() {
        cancelAnimationFrame(this.timer);
    }
    
    render() {
        return (
            <table className="table">
                <tbody>
                    {this.state.cells.map(this.renderCells)}
                </tbody>
            </table>
        );
    }
};

export default Board;
