import React from "react";
import "./App.css";
import Board from "./Board";

const App = () => (
  <div className="App">
    <header className="App-header">
      <h1 className="App-title">Game of Life</h1>
    </header>

    <Board />
  </div>
);

export default App;
