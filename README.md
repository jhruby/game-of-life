# Game of Life - React
- simple React implementation of Conway's Game of Life
- grid with 50 rows and 100 columns
- there is a Gosper glider gun in the top left corner, the rest is generated randomly every time you load the page
- live demo at https://jhruby.gitlab.io/game-of-life
